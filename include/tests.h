//
// Created by admin-ubuntu on 13.01.2022.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include <stdio.h>
#include "../include/mem.h"
#include "../include/mem_internals.h"

void test_1(struct block_header* heap);
void test_2(struct block_header* heap);
void test_3(struct block_header* heap);
void test_4(struct block_header* heap);
void test_5(struct block_header* heap);

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
