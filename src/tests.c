//
// Created by admin-ubuntu on 13.01.2022.
//

#include "../include/mem.h"
#include "../include/tests.h"

static struct block_header* block_get_header(void* mem){
    return (struct block_header*) ((uint8_t*) mem - offsetof(struct block_header, contents));
}


void test_1(struct block_header* heap){
    fprintf(stdout,"test 1\n");
    void* mem_min = _malloc(10); // = REGION_MIN_SIZE (<24)
    if(mem_min==NULL){
        fprintf(stdout,"test 1 failed:malloc return NULL\n");
    }
    debug_heap(stdout,heap);
    if(heap->capacity.bytes!=24 || heap->is_free!=false){
        fprintf(stdout,"test 1 failed:problem with minimal size or block is free\n");
    }
    _free(mem_min);

    void* mem = _malloc(1000);
    if(mem==NULL){
        fprintf(stdout,"test 1 failed:malloc return NULL\n");
    }
    debug_heap(stdout,heap);
    if(heap->capacity.bytes!=1000 || heap->is_free!=false){
        fprintf(stdout,"test 1 failed:problem with size or block is free\n");
    }
    fprintf(stdout,"test 1 passed\n");
    _free(mem);
}

void test_2(struct block_header* heap){
    fprintf(stdout,"test 2\n");
    void* mem_1 = _malloc(1000);
    void* mem_2 = _malloc(2000);
    if(mem_1==NULL || mem_2==NULL){
        fprintf(stdout,"test 2 FAILED: malloc return NULL\n");
    }
    _free(mem_1);
    debug_heap(stdout,heap);
    struct block_header* block_header_mem_1 = block_get_header(mem_1);
    struct block_header* block_header_mem_2 = block_get_header(mem_2);
    if(block_header_mem_1->is_free!=true || block_header_mem_2!=false){
        fprintf(stdout,"test 2 FAILED: memory free problem\n");
    }
    fprintf(stdout,"test 2 passed\n");
    _free(mem_2);
}

void test_3(struct block_header* heap){
    fprintf(stdout,"test 3\n");
    void* mem_1 = _malloc(1000);
    void* mem_2 = _malloc(2000);
    void* mem_3 = _malloc(3000);
    if(mem_1==NULL || mem_2==NULL || mem_3==NULL){
        fprintf(stdout,"test 3 FAILED: malloc return NULL\n");
    }
    _free(mem_2);
    _free(mem_1);
    debug_heap(stdout,heap);
    struct block_header* block_header_mem_1 = block_get_header(mem_1);
    struct block_header* block_header_mem_2 = block_get_header(mem_2);
    struct block_header* block_header_mem_3 = block_get_header(mem_3);
    if(block_header_mem_1->is_free!=true || block_header_mem_2->is_free!=true || block_header_mem_3->is_free!=false){
        fprintf(stdout,"test 3 FAILED: memory free problem\n");
    }
    if(block_header_mem_1->capacity.bytes!=3000+ offsetof(struct block_header, contents)){
        fprintf(stdout,"test 3 FAILED:problem with size\n");
    }
    fprintf(stdout,"test 3 passed\n");
    _free(mem_3);

}

void test_4(struct block_header* heap){
    fprintf(stdout,"test 4\n");
    void* mem_1 = _malloc(1000);
    void* mem_2 = _malloc(100);
    if(mem_1==NULL || mem_2==NULL){
        fprintf(stdout,"test 4 FAILED: malloc return NULL\n");
    }
    debug_heap(stdout,heap);
    struct block_header* block_header_mem_1 = block_get_header(mem_1);
    struct block_header* block_header_mem_2 = block_get_header(mem_2);
    if((uint8_t *)block_header_mem_1->contents+block_header_mem_1->capacity.bytes!=(uint8_t *)block_header_mem_2){
        fprintf(stdout,"test 4 FAILED: new block not after the previous");
    }
    fprintf(stdout,"test 4 passed");
    _free(mem_2);
    _free(mem_1);
}

void test_5(struct block_header* heap){
    fprintf(stdout,"test 5\n");
    struct block_header* addr = heap;
    while (addr->next!=NULL){
        addr=addr->next;
    }
    map_pages((uint8_t *) addr + size_from_capacity(addr->capacity).bytes, 1000, MAP_FIXED);
    void* mem_1 = _malloc(1000000);
    if(mem_1==NULL){
        fprintf(stdout,"test 5 FAILED: malloc return NULL\n");
    }
    debug_heap(stdout,heap);
    struct block_header* block_header_mem_1 = block_get_header(mem_1);
    if(block_header_mem_1==addr){
        fprintf(stdout,"test 5 FAILED: address match");
    }
    fprintf(stdout,"test 5 passed");
    _free(mem_1);
}
