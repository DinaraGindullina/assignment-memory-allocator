//
// Created by admin-ubuntu on 13.01.2022.
//
#include "../include/mem.h"
#include "../include/tests.h"


int main(){

    struct block_header* heap= (struct block_header*) heap_init(10000);
    test_1(heap);
    test_2(heap);
    test_3(heap);
    test_4(heap);
    test_5(heap);

    return  0;
}
