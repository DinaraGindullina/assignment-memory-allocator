#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "../include/mem.h"
#include "../include/mem_internals.h"
#include "../include/util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

// иниц-ем блок
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );


// получаем указатель на выд.память
void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    size_t length = region_actual_size(query);
    struct region rg;
    void* addr_for_region= map_pages(addr,length,MAP_FIXED_NOREPLACE);
    if(addr_for_region==MAP_FAILED){
        addr_for_region = map_pages(addr,length,0);
        rg.addr=addr_for_region;
        rg.size=length;
        rg.extends=false;

    }
    else{
        rg.addr=addr_for_region;
        rg.size=length;
        rg.extends=true;
    }
    block_size bs = {length};
    block_init(addr_for_region,bs,NULL);
    return rg;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ){
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

// проверяем можно ли разделить блок
static bool block_splittable( struct block_header* restrict block, size_t query){
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

// разбиваем блок на два меньших
static bool split_if_too_big( struct block_header* block, size_t query ){
    if(block_splittable(block,query)){
        void* addr_new_block = block+ offsetof(struct block_header,contents)+query;
        block_init(addr_new_block,(block_size){block->capacity.bytes-query},block->next);
        block->capacity=(block_capacity){query};
        block->next=addr_new_block;
        return true;
    }
    else{
        return false;
    }
}


/*  --- Слияние соседних свободных блоков --- */
// получаем указатель на следующий блок
static void* block_after( struct block_header const* block ){
    return  (void*) (block->contents + block->capacity.bytes);
}

// проверяем последовательны ли блоки
static bool blocks_continuous (struct block_header const* fst,struct block_header const* snd ){
    return (void*)snd == block_after(fst);
}

// проверяем можно ли объединить соседние блоки
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd){
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

// объединение блоков, если возможно
static bool try_merge_with_next( struct block_header* block ){
    if(block->next!=NULL && mergeable(block,block->next)){
        struct block_header* bh = block->next;
        block->next=bh->next;
        size_t length =((block->capacity.bytes)+size_from_capacity(bh->capacity).bytes);
//      block_capacity bc = {length};
        block->capacity=(block_capacity){length};
        return true;
    } else{
        return false;
    }
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED} type;
    struct block_header* block;
};

// ищем подходящий блок
static struct block_search_result find_good_or_last( struct block_header* restrict block, size_t sz ){
    struct block_search_result bsr;
    if(block==NULL){
        bsr.block=block;
        bsr.type=BSR_CORRUPTED;
        return bsr;
    }
    while(block){
        if(block->is_free && block_is_big_enough(sz,block)){
            bsr.block=block;
            bsr.type=BSR_FOUND_GOOD_BLOCK;
            return bsr;
        }
        // пытаемся объединить соседние блоки
        if(!try_merge_with_next(block)){
            block=block->next;
        }
    }
    bsr.block=block;
    bsr.type=BSR_REACHED_END_NOT_FOUND;
    return bsr;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing( size_t query, struct block_header* block ){
    struct block_search_result block_search_result = find_good_or_last(block,query);
    // попытаемся разбить блок на меньший
    if(block_search_result.type==BSR_FOUND_GOOD_BLOCK){
        split_if_too_big(block,query);
    }
    return block_search_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ){
    void* addr_new_block = block_after(last);
    struct region rg = alloc_region(addr_new_block,query);
    last->next=rg.addr;
    if(try_merge_with_next(last)){
        return last;
    } else{
        return last->next;
    }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start){
    query = size_max(query,BLOCK_MIN_CAPACITY);
    struct block_search_result block_search_result = try_memalloc_existing(query,heap_start);
    switch (block_search_result.type) {
        case BSR_REACHED_END_NOT_FOUND:
            block_search_result.block = grow_heap(heap_start,query);
            split_if_too_big(block_search_result.block,query);
            break;
        case BSR_CORRUPTED:
            return NULL;
        case BSR_FOUND_GOOD_BLOCK:
            break;
    }
    block_search_result.block->is_free=false;
    return block_search_result.block;
}

// выделяем память
void* _malloc( size_t query ){
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents){
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

// освобождаем память
void _free( void* mem ){
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    // пытаемся объединить освоб.блок с соседним
    while (try_merge_with_next(header)){}
}
